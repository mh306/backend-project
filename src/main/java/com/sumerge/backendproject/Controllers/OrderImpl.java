package com.sumerge.backendproject.Controllers;


import com.sumerge.backendproject.Interfaces.OrderContInterface;
import com.sumerge.backendproject.Resources.OrderRes;
import com.sumerge.backendproject.Services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OrderImpl implements OrderContInterface {

    @Autowired
    private OrderService OrderService;

    @Override
    public List<OrderRes> GetOrderAll() {
        return OrderService.GetOrderAll();
    }

    @Override
    public OrderRes GetOrderById(long OrderId) {
        return OrderService.GetOrderById(OrderId);
    }

    @Override
    public void AddOrder(OrderRes NewOrder) {
        OrderService.AddOrder(NewOrder);
    }

    @Override
    public void UpdateOrder(long OrderId,OrderRes NewOrder) {
        OrderService.UpdateOrder(OrderId,NewOrder);
    }

    @Override
    public void DeleteOrder(long Id) {
        OrderService.DeleteOrder(Id);
    }
}
