package com.sumerge.backendproject;

import com.sumerge.backendproject.Controllers.UserImpl;
import com.sumerge.backendproject.Exceptions.dataNotFoundException;
import com.sumerge.backendproject.Repositories.UserRepository;
import com.sumerge.backendproject.Resources.UserRes;
import com.sumerge.backendproject.Services.UserService;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.annotation.ComponentScan;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class UserTest {
    //to tell mockito to act as spring to inject dependencies needed by service
    @InjectMocks
    UserService userService;
    //mocks the repo for userservice to work
    @Mock
    UserRepository userRepository;

    //injects dependencies before each test
    @BeforeEach
    void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @DisplayName("Get User with invalid username")
    public void getUserInvTest(){
        when(userRepository.findById("heee")).thenThrow(dataNotFoundException.class);
        assertThrows(dataNotFoundException.class,()->{userRepository.findById("heee").get();},"User not found");
        verify(userRepository,times(1)).findById("heee");

    }

    @Test
    @DisplayName("Get User with valid username")
    public void getUserValTest(){
        //stubs user finding
        when(userRepository.findById("mh22")).thenReturn(java.util.Optional.of(new UserRes("mh22", "12")));
        Optional<UserRes> actual= userRepository.findById("mh22");
        assertEquals("mh22",actual.get().getUsername());
        assertEquals("12",actual.get().getPassword());
        verify(userRepository,times(1)).findById("mh22");

    }


    @Test
    @DisplayName("delete User with invalid username")
    public void updateUserInvTest(){
        assertThrows(dataNotFoundException.class, ()->userService.DeleteUser("36345"),"user not found !");
        verify(userRepository,times(0)).deleteById("36345");
    }

}
