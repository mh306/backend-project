package com.sumerge.backendproject;

import com.sumerge.backendproject.Exceptions.dataNotFoundException;
import com.sumerge.backendproject.Repositories.MenuItemRepository;
import com.sumerge.backendproject.Resources.MenuItemRes;
import com.sumerge.backendproject.Resources.UserRes;
import com.sumerge.backendproject.Services.MenuItemService;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.annotation.ComponentScan;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class MenuItemTest {
    //to tell mockito to act as spring to inject dependencies needed by service
    @InjectMocks
    MenuItemService MenuItemService;
    //mocks the repo for MenuItemService to work
    @Mock
    MenuItemRepository MenuItemRepository;

    //injects dependencies before each test
    @BeforeEach
    void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @DisplayName("Get MenuItem with invalid MenuItemId")
    public void getMenuItemInvTest(){
        when(MenuItemRepository.findById("chicken22")).thenThrow(dataNotFoundException.class);
        assertThrows(dataNotFoundException.class,()->{MenuItemRepository.findById("chicken22").get();},"MenuItem not found");
        verify(MenuItemRepository,times(1)).findById("chicken22");

    }

    @Test
    @DisplayName("Get MenuItem with valid MenuItemId")
    public void getMenuItemValTest(){
        //stubs MenuItem finding
        when(MenuItemRepository.findById("chicken22")).thenReturn(java.util.Optional.of(new MenuItemRes("chicken22","",20,"")));
        Optional<MenuItemRes> actual= MenuItemRepository.findById("chicken22");
        assertEquals("chicken22",actual.get().getName());
        assertEquals(20,actual.get().getPrice());
        verify(MenuItemRepository,times(1)).findById("chicken22");

    }


    @Test
    @DisplayName("delete MenuItem with invalid MenuItemId")
    public void updateMenuItemInvTest(){
        assertThrows(dataNotFoundException.class, ()->MenuItemService.DeleteMenuItem("chicken22"),"MenuItem not found !");
        verify(MenuItemRepository,times(0)).deleteById("chicken22");
    }

}
