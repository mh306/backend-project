package com.sumerge.backendproject.Resources;

import javax.persistence.*;

//@Table(name = "MenuItem")
@Entity
public class MenuItemRes {

    @Id
    private String Name;
    private String Weight;
    private int Price;
    private String Img;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public String getImg() {
        return Img;
    }

    public void setImg(String img) {
        Img = img;
    }

    public MenuItemRes(String name, String weight, int price, String img) {
        Name = name;
        Weight = weight;
        Price = price;
        Img = img;
    }

    public MenuItemRes() {
    }
}
