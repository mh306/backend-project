package com.sumerge.backendproject.Resources;

public class AuthenticationResponseRes {

    String JWT;

    public AuthenticationResponseRes(String JWT) {
        this.JWT = JWT;
    }

    public AuthenticationResponseRes() {
    }

    public String getJWT() {
        return JWT;
    }

    public void setJWT(String JWT) {
        this.JWT = JWT;
    }
}
