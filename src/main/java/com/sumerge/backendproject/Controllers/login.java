package com.sumerge.backendproject.Controllers;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.sumerge.backendproject.Resources.AuthenticationRequestRes;
import com.sumerge.backendproject.Resources.AuthenticationResponseRes;
import com.sumerge.backendproject.Resources.UserRes;
import com.sumerge.backendproject.Security.securityUtils;
import com.sumerge.backendproject.userDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class login {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private userDetailsService userDetailsService;

    @RequestMapping(method = RequestMethod.POST, value = "/authenticate")
    public ResponseEntity<?> login(@RequestBody AuthenticationRequestRes user) throws  Exception{
        try{
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(user.getUsername(),user.getPassword())
            );

        }catch(BadCredentialsException e){
            throw new Exception("Incorrect username or password !");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(user.getUsername());
        String JWT = com.auth0.jwt.JWT.create()
                .withSubject(user.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + securityUtils.EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(securityUtils.SECRET.getBytes()));

        return ResponseEntity.ok(new AuthenticationResponseRes(JWT));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/logoutuser")
    public String logout(){
        //SecurityContextHolder.clearContext();
        return "LoggedOut";
    }
}
