package com.sumerge.backendproject.Repositories;

import com.sumerge.backendproject.Resources.UserRes;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserRes,String> {
}
