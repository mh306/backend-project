package com.sumerge.backendproject.Interfaces;



import com.sumerge.backendproject.Resources.OrderRes;
import com.sumerge.backendproject.Resources.UserRes;
import com.sumerge.backendproject.Resources.UserRes;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/user")
public interface UserContInterface {

    //CRUD OPS METHODS

   @RequestMapping
    List<UserRes> GetUserAll();

   @RequestMapping("/{Username}")
    UserRes GetUserByUsername(@PathVariable("Username") String Username);

    @RequestMapping("/{Username}/orders")
    List<OrderRes> GetUserOrders(@PathVariable("Username") String Username);

    @RequestMapping(method = RequestMethod.POST)
    void AddUser(@RequestBody UserRes NewUser);

    @RequestMapping(method = RequestMethod.PUT,value = "/{Username}")
    void UpdateUser(@PathVariable("Username") String Username,@RequestBody UserRes NewUser);

    @RequestMapping(method = RequestMethod.DELETE, value = "/{Username}")
    void DeleteUser(@PathVariable("Username") String Username);


}