package com.sumerge.backendproject.Interfaces;

import com.sumerge.backendproject.Resources.MenuItemRes;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/menu")
public interface MenuItemContInterface {

    //CRUD OPS METHODS

    @RequestMapping
    List<MenuItemRes> GetMenuItemAll();

    @RequestMapping("/{MenuItemName}")
    MenuItemRes GetMenuItemByName(@PathVariable("MenuItemName") String ItemName);

    @RequestMapping(method = RequestMethod.POST)
    void AddMenuItem(@RequestBody MenuItemRes NewItem);

    @RequestMapping(method = RequestMethod.PUT,value = "/{MenuItemName}")
    void UpdateMenuItem(@PathVariable("MenuItemName") String MenuItemName,@RequestBody MenuItemRes NewItem);

    @RequestMapping(method = RequestMethod.DELETE, value = "/{MenuItemName}")
    void DeleteMenuItem(@PathVariable("MenuItemName") String MenuItemName);


}
