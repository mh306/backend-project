package com.sumerge.backendproject.Exceptions;

import com.sun.jndi.cosnaming.ExceptionMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class dataNotFoundExceptionWrapper   {
    @ExceptionHandler(value = dataNotFoundException.class)
    public ResponseEntity<Object> exception (dataNotFoundException exception){
        return new ResponseEntity<>("Data not found ", HttpStatus.NOT_FOUND);
    }
}
