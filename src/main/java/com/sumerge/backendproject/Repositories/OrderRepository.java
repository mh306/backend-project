package com.sumerge.backendproject.Repositories;

import com.sumerge.backendproject.Resources.OrderRes;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<OrderRes, Long> {
    List<OrderRes> findByUserUsername(String Username);
}
