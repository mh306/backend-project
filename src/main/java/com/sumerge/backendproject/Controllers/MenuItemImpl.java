package com.sumerge.backendproject.Controllers;

import com.sumerge.backendproject.Interfaces.MenuItemContInterface;
import com.sumerge.backendproject.Resources.MenuItemRes;
import com.sumerge.backendproject.Services.MenuItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController // DOESNT WORK IF THIS ANNOTATION IS ON INTERFACE ONLY !!!!!!!
public class MenuItemImpl implements MenuItemContInterface {

    @Autowired
    private MenuItemService MenuItemService;

    @Override
    public List<MenuItemRes> GetMenuItemAll() {
        return MenuItemService.GetMenuItemAll();
    }

    @Override
    public MenuItemRes GetMenuItemByName(String ItemName) {
        return MenuItemService.GetMenuItemByName(ItemName);
    }

    @Override
    public void AddMenuItem(MenuItemRes NewItem) {
        MenuItemService.AddMenuItem(NewItem);
    }

    @Override
    public void UpdateMenuItem(String MenuItemName,MenuItemRes NewMenuItem) {

        MenuItemService.UpdateMenuItem(MenuItemName,NewMenuItem);
    }

    @Override
    public void DeleteMenuItem(String MenuItemName) {
        MenuItemService.DeleteMenuItem(MenuItemName);
    }
}
