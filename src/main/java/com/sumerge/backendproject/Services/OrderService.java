package com.sumerge.backendproject.Services;

import com.sumerge.backendproject.Exceptions.dataNotFoundException;
import com.sumerge.backendproject.Repositories.OrderRepository;
import com.sumerge.backendproject.Repositories.UserRepository;
import com.sumerge.backendproject.Resources.OrderRes;
import com.sumerge.backendproject.Resources.OrderRes;
import com.sumerge.backendproject.Resources.UserRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {
    @Autowired
    OrderRepository OrderRepository;
    @Autowired
    UserRepository userRepository;

    public List<OrderRes> GetOrderAll() {
        List<OrderRes>  OrderList = new ArrayList<>();
        OrderRepository.findAll().forEach(OrderList::add);
        return OrderList;
    }

    public OrderRes GetOrderById(long OrderId) {
        Optional<OrderRes> order= OrderRepository.findById(OrderId);
        if(order.isPresent())
            return order.get();
        else
            throw new dataNotFoundException();

    }

    public void AddOrder(OrderRes NewOrder) {
        OrderRepository.save(NewOrder);
    }

    public void UpdateOrder(long OrderId,OrderRes NewOrder) {
        Optional<OrderRes> order= OrderRepository.findById(OrderId);
        if(order.isPresent())
        {
            NewOrder.setID(OrderId);
            OrderRepository.save(NewOrder);
        }
        else
            throw new dataNotFoundException();

    }

    public void DeleteOrder(long Id) {
        Optional<OrderRes> order= OrderRepository.findById(Id);
        if(order.isPresent())
            OrderRepository.deleteById(Id);
        else
            throw new dataNotFoundException();


    }

    public List<OrderRes> GetOrderHistoryForUser(String Username){

        Optional<UserRes> user= userRepository.findById(Username);
        if(user.isPresent())
            return OrderRepository.findByUserUsername(Username);
        else
            throw new dataNotFoundException();

    }


}
