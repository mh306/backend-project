package com.sumerge.backendproject.Interfaces;

import com.sumerge.backendproject.Resources.OrderRes;
import com.sumerge.backendproject.Resources.OrderRes;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/order")
public interface OrderContInterface {

    //CRUD OPS METHODS

    //RequestMapping("/") to make endpoint /order/ not /order
    @RequestMapping
    List<OrderRes> GetOrderAll();

    @RequestMapping("/{OrderId}")
    OrderRes GetOrderById(@PathVariable("OrderId") long OrderId);

    @RequestMapping(method = RequestMethod.POST)
    void AddOrder(@RequestBody OrderRes NewOrder);

    @RequestMapping(method = RequestMethod.PUT,value="{OrderId}")
    public void UpdateOrder(@PathVariable("OrderId") long OrderId, @RequestBody  OrderRes NewOrder);

    @RequestMapping(method = RequestMethod.DELETE, value = "/{OrderId}")
    void DeleteOrder(@PathVariable("OrderId") long Id);


}