package com.sumerge.backendproject;

import com.sumerge.backendproject.Exceptions.dataNotFoundException;
import com.sumerge.backendproject.Repositories.OrderRepository;
import com.sumerge.backendproject.Resources.OrderRes;
import com.sumerge.backendproject.Resources.UserRes;
import com.sumerge.backendproject.Services.OrderService;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.annotation.ComponentScan;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class OrderTest {
    //to tell mockito to act as spring to inject dependencies needed by service
    @InjectMocks
    OrderService orderService;
    //mocks the repo for orderService to work
    @Mock
    OrderRepository orderRepository;

    //injects dependencies before each test
    @BeforeEach
    void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @DisplayName("Get order with invalid orderId")
    public void getorderInvTest(){
        when(orderRepository.findById((long) 565)).thenThrow(dataNotFoundException.class);
        assertThrows(dataNotFoundException.class,()->{orderRepository.findById((long) 565).get();},"order not found");
        verify(orderRepository,times(1)).findById((long) 565);

    }

    @Test
    @DisplayName("Get order with valid orderId")
    public void getorderValTest(){
        //stubs order finding
        when(orderRepository.findById((long) 565)).thenReturn(java.util.Optional.of(new OrderRes(1,1000,new UserRes(),new ArrayList<>())));
        Optional<OrderRes> actual= orderRepository.findById((long) 565);
        assertEquals(1,actual.get().getID());
        assertEquals(1000,actual.get().getPrice());
        verify(orderRepository,times(1)).findById((long) 565);

    }


    @Test
    @DisplayName("delete order with invalid orderId")
    public void updateorderInvTest(){
        assertThrows(dataNotFoundException.class, ()->orderService.DeleteOrder((long) 565),"order not found !");
        verify(orderRepository,times(0)).deleteById((long) 565);
    }

}
