package com.sumerge.backendproject.Repositories;

import com.sumerge.backendproject.Resources.MenuItemRes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//use this annotation only if table name is different than default
//@RepositoryRestResource(collectionResourceRel = "people", path = "people")
public interface MenuItemRepository extends CrudRepository<MenuItemRes,String> {
}
