package com.sumerge.backendproject.Services;

import com.sumerge.backendproject.Exceptions.dataNotFoundException;
import com.sumerge.backendproject.Repositories.OrderRepository;
import com.sumerge.backendproject.Repositories.UserRepository;
import com.sumerge.backendproject.Resources.OrderRes;
import com.sumerge.backendproject.Resources.UserRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository UserRepository;
    @Autowired
    OrderRepository OrderRepository;
    //creates new object from bean
    //@Autowired
  // BCryptPasswordEncoder pwEncoder;

    public List<UserRes> GetUserAll() {
        List<UserRes>  UserList = new ArrayList<>();
        UserRepository.findAll().forEach(UserList::add);
        return UserList;
    }

    public UserRes GetUserByUsername(String UserName) {
        //id here is PK which is name string
        Optional<UserRes> user= UserRepository.findById(UserName);
        if(user.isPresent())
            return user.get();
        else
            throw new dataNotFoundException();
    }

    public List<OrderRes> GetUserOrders(String username) {

        //check if user valid
        Optional<UserRes> user= UserRepository.findById(username);
        if(user.isPresent())
            return OrderRepository.findByUserUsername(username);
        else
            throw new dataNotFoundException();
    }
    public void AddUser(UserRes NewUser) {
        //Hashes PW
      //  NewUser.setPassword(pwEncoder.encode(NewUser.getPassword()));
        UserRepository.save(NewUser);
    }

    public void UpdateUser(String UserName,UserRes NewUser) {
        Optional<UserRes> user= UserRepository.findById(UserName);
        if(user.isPresent()) {
            NewUser.setUsername(UserName);
            UserRepository.save(NewUser);
        }
        else
            throw new dataNotFoundException();


    }

    public void DeleteUser(String Name) {

        Optional<UserRes> user= UserRepository.findById(Name);
        if(user.isPresent())
            UserRepository.deleteById(Name);
        else
            throw new dataNotFoundException();
    }


}
