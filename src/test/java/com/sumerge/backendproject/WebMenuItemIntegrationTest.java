//package com.sumerge.backendproject;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.sumerge.backendproject.Repositories.UserRepository;
//import com.sumerge.backendproject.Resources.UserRes;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static org.hamcrest.Matchers.hasSize;
//import static org.hamcrest.Matchers.is;
//import static org.springframework.mock.http.server.reactive.MockServerHttpRequest.post;
//import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@ExtendWith(SpringExtension.class)
//@SpringBootTest
//@AutoConfigureMockMvc
//class WebMenuItemIntegrationTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Autowired
//    private ObjectMapper objectMapper;
//
//    @Autowired
//    private UserRepository userRepository;
//
//    @Test
//    void registrationWorksThroughAllLayers() throws Exception {
//        UserRes user = new UserRes("Zaphod", "zaphod@galaxy.net");
//
//        mockMvc.perform(post("/authenticate")
//                .contentType(MediaType.APPLICATION_JSON)
//                .body("{\"Username\":\"foo\",\"password\":\"foo\"}")
//                .(status().isOk()));
//
//        mockMvc.perform(post("/forums/{forumId}/register", 42L)
//                .contentType("application/json")
//                .param("sendWelcomeMail", "true")
//                .content(objectMapper.writeValueAsString(user)))
//                .andExpect(status().isOk());
//
//        mockMvc.perform(get("/api/employees")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$", hasSize(1)))
//                .andExpect(jsonPath("$[0].name", is(user.getName())));
//        UserRes userEntity = userRepository.findByName("Zaphod");
//        assertThat(userEntity.getEmail()).isEqualTo("zaphod@galaxy.net");
//    }
//
//}