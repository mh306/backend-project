package com.sumerge.backendproject.Controllers;


import com.sumerge.backendproject.Interfaces.UserContInterface;
import com.sumerge.backendproject.Resources.OrderRes;
import com.sumerge.backendproject.Resources.UserRes;
import com.sumerge.backendproject.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserImpl implements UserContInterface {

    @Autowired
    private UserService UserService;

    @Override
    public List<UserRes> GetUserAll() {
        return UserService.GetUserAll();
    }

    @Override
    public UserRes GetUserByUsername(String Username) {
        return UserService.GetUserByUsername(Username);
    }

    @Override
    public List<OrderRes> GetUserOrders(String Username) {
        //System.out.println(Username+"from contr");
        return UserService.GetUserOrders(Username);
    }

    @Override
    public void AddUser(UserRes NewUser) {
    UserService.AddUser(NewUser);
    }

    @Override
    public void UpdateUser(String Username, UserRes NewUser) {
    UserService.UpdateUser(Username,NewUser);
    }

    @Override
    public void DeleteUser(String Username) {
    UserService.DeleteUser(Username);
    }


}
