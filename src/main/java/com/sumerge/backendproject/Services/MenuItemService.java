package com.sumerge.backendproject.Services;

import com.sumerge.backendproject.Exceptions.dataNotFoundException;
import com.sumerge.backendproject.Repositories.MenuItemRepository;
import com.sumerge.backendproject.Resources.MenuItemRes;
import com.sumerge.backendproject.Resources.UserRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MenuItemService {

    @Autowired
    MenuItemRepository MenuItemRepo;

    public List<MenuItemRes> GetMenuItemAll() {
        List<MenuItemRes>  MenuItemList = new ArrayList<>();
       // System.out.println("getting");
        MenuItemRepo.findAll().forEach(item ->{
            MenuItemList.add(item);
          //  System.out.println("NAME ISSS: "+item.getName());
        });
        return MenuItemList;
    }

    public MenuItemRes GetMenuItemByName(String ItemName) {
        //id here is PK which is name string
        Optional<MenuItemRes> menuItem= MenuItemRepo.findById(ItemName);
        if(menuItem.isPresent()) {
            return  menuItem.get();
        }
        else
            throw new dataNotFoundException();
    }

    public void AddMenuItem(MenuItemRes NewItem) {
        MenuItemRepo.save(NewItem);
    }

    public void UpdateMenuItem(String MenuItemName,MenuItemRes NewItem) {

        Optional<MenuItemRes> menuItem= MenuItemRepo.findById(MenuItemName);
        if(menuItem.isPresent()) {
            NewItem.setName(MenuItemName);
            MenuItemRepo.save(NewItem);
        }
        else
            throw new dataNotFoundException();
    }

    public void DeleteMenuItem(String Name) {
        Optional<MenuItemRes> menuItem= MenuItemRepo.findById(Name);
        if(menuItem.isPresent()) {

            MenuItemRepo.deleteById(Name);
        }
        else
            throw new dataNotFoundException();

    }
}
