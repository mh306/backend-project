package com.sumerge.backendproject.Resources;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;


@Entity
public class UserRes {

    @Id
    private String username;
    private String Password;
    //private List<OrderRes> OrderHistory;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

//    public List<OrderRes> getOrderHistory() {
//        return OrderHistory;
//    }
//
//    public void setOrderHistory(List<OrderRes> orderHistory) {
//        OrderHistory = orderHistory;
//    }
//
//    public void AddOrderToHistory(OrderRes NewOrder){
//        this.OrderHistory.add(NewOrder);
//    }

    public UserRes(String username, String password) {
        this.username = username;
        Password = password;
        //OrderHistory = orderHistory;
    }

    public UserRes() {
    }
}
