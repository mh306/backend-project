package com.sumerge.backendproject.Resources;

import javax.persistence.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@Table(name = "Order")
@Entity
public class OrderRes {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long ID;
    private int Price;
    @ManyToOne
    private UserRes user;
    @ManyToMany
    List<MenuItemRes> OrderItems = new ArrayList<MenuItemRes>();

    public List<MenuItemRes> getOrderItems() {
        return OrderItems;
    }

    public void setOrderItems(List<MenuItemRes> orderItems) {
        OrderItems = orderItems;
    }



    public UserRes getUser() {
        return user;
    }

    public void setUser(UserRes user) {
        this.user = user;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }


    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }


    public OrderRes(long ID, int price, UserRes user, List<MenuItemRes> orderItems) {
        this.ID = ID;
        Price = price;
        this.user = user;
        OrderItems = orderItems;
    }

    public OrderRes() {
    }

}
